package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class IO {

    public ArrayList<Double> informarLado() {
        ArrayList<Double> listaLados = new ArrayList<>();
        double tamanhoLado = 0;
        for (int i = 0; i < 3; i++) {
            System.out.println("Digite o tamanho do lado: (digite 'q' para sair)");
            Scanner scanner = new Scanner(System.in);
            if (listaLados.size() < 3) {
                String varEntrada = scanner.next();

                if (varEntrada.equals("q")) {
                    System.out.println("Você finalizou a criação da sua forma geometrica.");
                    i = 100;
                } else {
                    tamanhoLado = Double.valueOf(varEntrada);
                    listaLados.add(tamanhoLado);
                }
            } else {
                 i = 100;
            }
        }
        return listaLados;
    }
}
