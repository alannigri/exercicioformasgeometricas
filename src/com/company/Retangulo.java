package com.company;

import java.util.ArrayList;

public class Retangulo extends FormaGeometrica {
    double lado1;
    double lado2;

    public Retangulo(ArrayList<Double> lista) {
        this.lado1 = lista.get(0);
        this.lado2 = lista.get(1);
    }


    @Override
    public void calcularArea() {
        area = lado1 * lado2;
        System.out.println("A área do seu retangulo é de: " + area);

    }

}
