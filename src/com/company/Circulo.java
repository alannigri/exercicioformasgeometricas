package com.company;

import java.util.ArrayList;

public class Circulo extends FormaGeometrica {
    double lado1;

    public Circulo(ArrayList<Double> lista) {
        this.lado1 = lista.get(0);
    }

    @Override
    public void calcularArea() {
        area = Math.pow(this.lado1, 2) * Math.PI;
        System.out.println("A área do seu círculo é de: " + area);

    }

}
