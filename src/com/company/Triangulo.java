package com.company;

import java.util.ArrayList;

public class Triangulo extends FormaGeometrica {
    double lado1;
    double lado2;
    double lado3;

    public Triangulo(ArrayList<Double> lista) {
        this.lado1 = lista.get(0);
        this.lado2 = lista.get(1);
        this.lado3 = lista.get(2);
    }

    public boolean validaTriangulo() {
        if ((lado1 + lado2) > lado3 && (lado1 + lado3) > lado2 && (lado2 + lado3 > lado1)) {
            return true;
        } else {
            System.out.println("ERRO!!! Triangulo invalido");
            return false;
        }
    }

    @Override
//    public void calcularArea(double lado1, double lado2, double lado3) {
    public void calcularArea() {
        double s = (lado1 + lado2 + lado3) / 2;
        area = Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
        System.out.println("A área do seu triangulo é de: " + area);

    }


}
