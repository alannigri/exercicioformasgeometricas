package com.company;

import java.util.ArrayList;

public class ValidarForma extends FormaGeometrica {

//    @Override
    public void validaForma(int numero) {

    }

    public void defineForma(ArrayList<Double> lista) {
        if (lista.size() == 0) {
            System.out.println("Quantidade de lados inválido.");
        }

        if (lista.size() == 1) {
            Circulo circulo = new Circulo(lista);
            circulo.calcularArea();

        } else if (lista.size() == 2) {
            Retangulo retangulo = new Retangulo(lista);
            retangulo.calcularArea();

        } else if (lista.size() == 3) {
            Triangulo triangulo = new Triangulo(lista);
            if (triangulo.validaTriangulo()) {
                  triangulo.calcularArea();
            }
        }
    }

    @Override
    public void calcularArea() {

    }
}